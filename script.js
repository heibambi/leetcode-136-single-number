var singleNumber = function(nums) {
    let map = {};
    for (let i = 0 ; i < nums.length ; i++){
        if(map[nums[i]]){
            delete map[nums[i]]
        } else {
            map[nums[i]] = true;
        }
    }

    return Object.keys(map)[0];

};

var singleNumberV2 = function(nums) {
    return nums.reduce((acc, cur) => acc ^ cur);
};

const answer1 = singleNumber([2,2,1]);
const answer2 = singleNumber([4,1,2,1,2]);
const answer3 = singleNumber([1]);

console.log({
    answer1,
    answer2,
    answer3
})